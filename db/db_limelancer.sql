-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 04, 2020 at 11:24 AM
-- Server version: 5.6.49-log
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `limelancer`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) UNSIGNED DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Design 2', 0, '2020-10-04', '2020-10-04'),
(3, 'Hey', NULL, '2020-10-04', '2020-10-04'),
(6, 'hello', 1, '2020-10-04', '2020-10-04');

-- --------------------------------------------------------

--
-- Table structure for table `sys_district`
--

CREATE TABLE `sys_district` (
  `district_id` int(11) NOT NULL,
  `district_name` varchar(100) NOT NULL,
  `division_id` int(11) NOT NULL COMMENT 'FK sys_division',
  `district_code` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_district`
--

INSERT INTO `sys_district` (`district_id`, `district_name`, `division_id`, `district_code`, `status`, `created_by`, `updated_by`, `created_on`, `updated_at`) VALUES
(1, 'Dhaka', 1, '11', 1, NULL, NULL, '2020-05-02 16:24:39', NULL),
(2, 'Narayangong', 1, '152', 1, NULL, NULL, '2020-05-02 16:33:45', NULL),
(3, 'Cumilla', 2, '123', 1, NULL, NULL, '2020-05-02 16:33:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_division`
--

CREATE TABLE `sys_division` (
  `division_id` int(11) NOT NULL,
  `division_name` varchar(100) NOT NULL,
  `division_code` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_division`
--

INSERT INTO `sys_division` (`division_id`, `division_name`, `division_code`, `status`, `created_by`, `updated_by`, `created_on`, `updated_at`) VALUES
(1, 'Dhaka', '101', 1, NULL, NULL, '2020-05-02 14:56:37', NULL),
(2, 'Cumilla', '102', 1, NULL, NULL, '2020-05-02 14:56:45', NULL),
(3, 'Chittagong', '1111', 1, NULL, NULL, '2020-05-02 14:59:44', NULL),
(4, 'Rajshahi', '11', 1, NULL, NULL, '2020-05-02 15:06:39', NULL),
(5, 'Barishal', '11', 1, NULL, NULL, '2020-05-02 15:08:37', NULL),
(6, 'Rangpur', '44', 1, NULL, NULL, '2020-05-02 15:08:50', NULL),
(7, 'Sylhet', '999', 1, NULL, NULL, '2020-05-02 15:10:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_upazila`
--

CREATE TABLE `sys_upazila` (
  `upazila_id` int(11) NOT NULL,
  `upazila_name` varchar(30) NOT NULL,
  `district_id` int(11) NOT NULL COMMENT 'FK sys_district',
  `upazila_code` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_upazila`
--

INSERT INTO `sys_upazila` (`upazila_id`, `upazila_name`, `district_id`, `upazila_code`, `status`, `created_by`, `updated_by`, `created_on`, `updated_at`) VALUES
(1, 'Dhanmondi', 1, '11', 1, NULL, NULL, '2020-05-02 17:06:08', NULL),
(2, 'Mohammadpur', 1, '103', 1, NULL, NULL, '2020-05-02 17:07:34', NULL),
(3, 'Moinamoti', 3, '985', 1, NULL, NULL, '2020-05-02 17:07:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `nav_id` int(11) NOT NULL COMMENT 'FK user_nav_menu',
  `style_css` varchar(100) DEFAULT NULL,
  `has_sub_menu` tinyint(1) DEFAULT NULL,
  `menu_link` varchar(255) DEFAULT NULL,
  `menu_sl` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`menu_id`, `menu_name`, `nav_id`, `style_css`, `has_sub_menu`, `menu_link`, `menu_sl`, `status`, `created_by`, `updated_by`, `created_on`, `updated_at`) VALUES
(1, 'Demo', 2, NULL, 1, NULL, 1, 1, 1, NULL, '2020-05-09 16:04:40', NULL),
(2, 'User', 3, NULL, 1, NULL, 1, 1, 1, NULL, '2020-05-09 16:12:48', NULL),
(3, 'Menu', 3, NULL, 1, NULL, 2, 1, 1, NULL, '2020-05-09 16:12:48', NULL),
(4, 'Address', 3, NULL, 1, NULL, 3, 1, 3, NULL, '2020-05-09 16:28:12', NULL),
(5, 'Test Page', 4, NULL, 0, 'test_report', 1, 1, 3, NULL, '2020-05-09 16:35:06', NULL),
(6, 'Demo', 5, NULL, 1, NULL, 1, 1, 1, NULL, '2020-05-10 01:39:07', NULL),
(7, 'Demo', 5, NULL, 1, NULL, 2, 1, 1, NULL, '2020-05-10 01:41:38', NULL),
(8, 'Demo', 5, NULL, 1, NULL, 3, 1, 1, NULL, '2020-05-10 01:41:57', NULL),
(9, 'Demo', 6, NULL, 1, NULL, 1, 1, 1, NULL, '2020-05-10 01:43:07', NULL),
(10, 'Demo', 2, NULL, 1, NULL, 2, 1, 1, NULL, '2020-06-27 12:08:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_nav_menu`
--

CREATE TABLE `user_nav_menu` (
  `nav_id` int(11) NOT NULL,
  `nav_name` varchar(100) NOT NULL,
  `style_class` varchar(100) DEFAULT NULL,
  `has_menu` tinyint(1) DEFAULT NULL,
  `nav_link` varchar(255) DEFAULT NULL,
  `nav_sl` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_nav_menu`
--

INSERT INTO `user_nav_menu` (`nav_id`, `nav_name`, `style_class`, `has_menu`, `nav_link`, `nav_sl`, `status`, `created_by`, `updated_by`, `created_on`, `updated_at`) VALUES
(1, 'Dashboard', 'fa fa-th-large', 0, 'dashboard', 1, 1, 1, NULL, '2020-05-03 17:58:49', NULL),
(2, 'Settings', 'fas fa-cog fa-fw', 1, NULL, 2, 1, 1, NULL, '2020-05-03 18:00:39', NULL),
(3, 'Config', 'fas fa-sync fa-spin', 1, NULL, 3, 1, 1, 3, '2020-05-03 18:00:39', NULL),
(4, 'Reports', 'fas fa-book fa-fw', 1, NULL, 6, 1, 3, 3, '2020-05-09 12:46:31', NULL),
(5, 'Demo', 'fa fa-info-circle', 1, NULL, 4, 1, 3, 3, '2020-05-09 17:18:28', NULL),
(6, 'Demo', 'fa fa-dollar-sign', 1, NULL, 5, 1, 3, 3, '2020-05-09 17:18:41', NULL),
(7, 'Category', 'fa fa-files', 0, 'category', 8, 1, 1, 1, '2020-10-04 01:58:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`role_id`, `role_name`, `status`, `created_by`, `updated_by`, `created_on`, `updated_at`) VALUES
(1, 'Role 1', 1, NULL, 1, '2020-05-02 13:51:44', NULL),
(2, 'Role 2', 1, NULL, 1, '2020-05-02 14:25:54', NULL),
(3, 'Role 3', 2, NULL, 1, '2020-05-02 14:26:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `sub_menu_id` int(11) NOT NULL,
  `sub_menu_name` varchar(100) NOT NULL,
  `menu_id` int(11) NOT NULL COMMENT 'FK user_menu',
  `style_css` varchar(100) DEFAULT NULL,
  `sub_menu_link` varchar(255) DEFAULT NULL,
  `sub_menu_sl` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`sub_menu_id`, `sub_menu_name`, `menu_id`, `style_css`, `sub_menu_link`, `sub_menu_sl`, `status`, `created_by`, `updated_by`, `created_on`, `updated_at`) VALUES
(5, 'User Type', 2, NULL, 'user_type', 1, 1, 1, NULL, '2020-05-09 16:20:35', NULL),
(6, 'User Roles', 2, NULL, 'user_roles', 2, 1, 1, NULL, '2020-05-09 16:20:35', NULL),
(7, 'Navigation Bar', 3, NULL, 'nav_menu', 1, 1, 1, NULL, '2020-05-09 16:24:37', NULL),
(8, 'Menu bar', 3, NULL, 'menu', 2, 1, 1, NULL, '2020-05-09 16:24:37', NULL),
(9, 'Sub-Menu', 3, NULL, 'sub_menu', 3, 1, 1, NULL, '2020-05-09 16:24:37', NULL),
(10, 'Division', 4, NULL, 'division', 1, 1, 3, NULL, '2020-05-09 16:28:57', NULL),
(11, 'District', 4, NULL, 'district', 2, 1, 3, NULL, '2020-05-09 16:29:14', NULL),
(12, 'Upazila', 4, NULL, 'upazila', 3, 1, 3, NULL, '2020-05-09 16:29:34', NULL),
(16, 'User Manager', 2, NULL, 'users', 3, 1, 3, NULL, '2020-05-09 16:50:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `id` int(11) NOT NULL,
  `utype_name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT '1',
  `updated_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `utype_name`, `status`, `created_by`, `updated_by`, `created_on`, `updated_at`) VALUES
(1, 'Super Admin (IT)', 1, 1, NULL, '2020-02-04 15:44:27', NULL),
(2, 'Super Admin ( Administrative )', 1, 1, 1, '2020-02-04 15:44:27', NULL),
(3, 'Demo 1', 1, 1, 1, '2020-02-04 16:46:46', NULL),
(4, 'Demo 2', 1, 1, 1, '2020-02-04 16:46:54', NULL),
(5, 'Demo 3', 1, 1, 1, '2020-02-04 16:47:49', NULL),
(6, 'Demo 4', 1, 1, 1, '2020-02-04 16:57:56', NULL),
(7, 'Demo 6', 1, 1, 1, '2020-02-05 10:47:07', NULL),
(8, 'Demo 7', 1, 1, 1, '2020-02-05 11:43:38', NULL),
(10, 'Demo 8', 2, 1, 1, '2020-02-06 16:10:23', NULL),
(11, 'Demo 9', 1, 1, 1, '2020-04-28 14:03:10', NULL),
(12, 'Demo 10', 1, 1, 1, '2020-04-28 14:03:10', NULL),
(13, 'Demo 11', 1, 1, 1, '2020-04-29 17:03:52', NULL),
(14, 'Demo 12', 1, 1, 1, '2020-04-29 17:04:49', NULL),
(15, 'Demo 13', 1, 3, 1, '2020-05-09 16:43:13', NULL),
(16, 'Demo 14', 1, 1, 1, '2020-05-10 01:30:10', NULL),
(17, 'Demo 15', 1, 1, 1, '2020-05-10 01:30:20', NULL),
(18, 'Demo 16', 1, 1, 1, '2020-05-10 01:30:39', NULL),
(19, 'Demo 17', 1, 1, 1, '2020-05-10 01:30:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_user`
--

CREATE TABLE `user_user` (
  `user_id` int(11) NOT NULL,
  `utype_id` int(11) NOT NULL COMMENT 'FK user_type',
  `user_photo` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_user`
--

INSERT INTO `user_user` (`user_id`, `utype_id`, `user_photo`, `user_name`, `password`, `status`, `created_by`, `updated_by`, `created_on`, `updated_at`) VALUES
(1, 1, '1505034887.jpg', 'polash', '25f9e794323b453885f5181f1b624d0b', 1, 1, 1, '2020-02-04 09:37:48', '2020-10-03 19:53:52'),
(2, 1, NULL, 'nafis', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 1, '2020-04-29 07:29:31', '2020-09-13 14:22:05'),
(3, 1, NULL, 'abc', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 1, '2020-05-03 06:56:50', '2020-09-13 14:22:13'),
(4, 3, NULL, 'demo', 'd41d8cd98f00b204e9800998ecf8427e', 1, 1, 1, '2020-05-03 08:19:08', '2020-09-13 14:22:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_district`
--
ALTER TABLE `sys_district`
  ADD PRIMARY KEY (`district_id`);

--
-- Indexes for table `sys_division`
--
ALTER TABLE `sys_division`
  ADD PRIMARY KEY (`division_id`);

--
-- Indexes for table `sys_upazila`
--
ALTER TABLE `sys_upazila`
  ADD PRIMARY KEY (`upazila_id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `user_nav_menu`
--
ALTER TABLE `user_nav_menu`
  ADD PRIMARY KEY (`nav_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`sub_menu_id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_user`
--
ALTER TABLE `user_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sys_district`
--
ALTER TABLE `sys_district`
  MODIFY `district_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sys_division`
--
ALTER TABLE `sys_division`
  MODIFY `division_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sys_upazila`
--
ALTER TABLE `sys_upazila`
  MODIFY `upazila_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_nav_menu`
--
ALTER TABLE `user_nav_menu`
  MODIFY `nav_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `sub_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_user`
--
ALTER TABLE `user_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
