@extends('master')
@section('main_content')

 
<div class="cat-menu">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>


            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Graphics & Design</a></li>
                    <li><a href="#">Digital Marketing</a></li>
                    <li><a href="#">Fun & Lifestyle </a></li>
                    <li><a href="#">Business</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Writing & Translation <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header">Nav header</li>
                            <li><a href="#">Separated link</a></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>

                    <li><a href="#">Programming & Tech</a></li>
                    <li><a href="#"> Video & Animation</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Writing & Translation <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header">Nav header</li>
                            <li><a href="#">Separated link</a></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>

            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
</div>

<section class="gig-details">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="sidebar">
                    <div class="single-sec">
                        <div class="tab-menu">

                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Premium</a></li>
                                <li><a data-toggle="tab" href="#menu1">Standard</a></li>
                                <li><a data-toggle="tab" href="#menu2">Basic</a></li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="s-1">
                                    <div class="t-info"><span class="title">FOUR DESIGNS </span><span
                                            class="pull-right price">$115</span></div>
                                    <p class="details">4 concepts, double-sided design, unlimited revisions, and high
                                        resolution pdf files ready to print! </p>
                                </div>
                                <div class="delivery">
                                    <i class="fa fa-clock-o"></i> 6 days Delivery <i class="fa fa-refresh"
                                                                                     aria-hidden="true"></i> unlimited
                                    Revisions

                                     
                                    <button class="btn  btn-block learn_btn" style="margin-top: 15px;">Continue ( 115$ )</button>
                                    <div class="text-center cp">
                                        <a href="">Compare Packages</a>
                                    </div>
                                </div>


                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <div class="s-1">
                                    <div class="t-info"><span class="title">FOUR DESIGNS </span><span
                                            class="pull-right price">$90</span></div>
                                    <p class="details">4 concepts, double-sided design, unlimited revisions, and high
                                        resolution pdf files ready to print! </p>
                                </div>
                                <div class="delivery">
                                    <i class="fa fa-clock-o"></i> 6 days Delivery <i class="fa fa-refresh"
                                                                                     aria-hidden="true"></i> unlimited
                                    Revisions

                                    <ul class="serv-list">
                                        <li><i class="fa fa-check" aria-hidden="true"></i> 4 Concepts</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Double-Sided</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> 4 Concepts</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Double-Sided</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Print-Ready</li>
                                    </ul>
                                    <button class="btn  btn-block learn_btn">Continue ( 90$ )</button>
                                    <div class="text-center cp">
                                        <a href="">Compare Packages</a>
                                    </div>
                                </div>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <div class="s-1">
                                    <div class="t-info"><span class="title">FOUR DESIGNS </span><span
                                            class="pull-right price">$100</span></div>
                                    <p class="details">4 concepts, double-sided design, unlimited revisions, and high
                                        resolution pdf files ready to print! </p>
                                </div>
                                <div class="delivery">
                                    <i class="fa fa-clock-o"></i> 6 days Delivery <i class="fa fa-refresh"
                                                                                     aria-hidden="true"></i> unlimited
                                    Revisions

                                    <ul class="serv-list">
                                        <li><i class="fa fa-check" aria-hidden="true"></i> 4 Concepts</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Double-Sided</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> 4 Concepts</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Double-Sided</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Print-Ready</li>
                                    </ul>
                                    <button class="btn  btn-block learn_btn">Continue ( 100$ )</button>
                                    <div class="text-center cp">
                                        <a href="">Compare Packages</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-sec">
                        <div class="user-img">
                            <img src="{{asset('front_assets/image/pexels-photo-1264210.jpeg')}}" alt="" class="img-circle img-thumbnail img-responsive"/>
                        </div>
                        <div class="uname"><a href="">jon.doekaz</a></div>
                        <div class="review text-center">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <div class="r-count">5.0 ( 4 reviews )</div>
                            <button class="contact btn learn_btn">Contact Me</button>
                        </div>
                        <div class="user-summary">
                            <div class="info"><i class="fa fa-map-marker"></i> From <span
                                    class="pull-right">Canada</span></div>
                            <div class="info"><i class="fa fa-user"></i> Member since <span
                                    class="pull-right">May 2018</span></div>
                            <div class="info"><i class="fa fa-clock-o"></i> Avg. Response Time <span class="pull-right">6 hours</span>
                            </div>
                            <div class="info"><i class="fa fa-location-arrow"></i> Recent Delivery <span
                                    class="pull-right">2 months</span></div>

                            <p>My name is Megan, and I'm a designer living in Vancouver, Canada. My creative practice is
                                very interdisciplinary, with my greatest interests being digital collage, ink
                                illustrations, video performance, and brand design. My work is always clean, refined,
                                and thought out right down to the smallest details. I'm open to all creative challenges,
                                but am particularly passionate about working with queer, trans, intersectional feminist,
                                vegan, or magical businesses. Please get in touch - I'd love to hear your ideas!
                            </p>
                        </div>


                    </div>
                </div>
            </div>

            <div class="col-md-8">

                                    

	<div class="gig-dtails gig-top">
		<div class="t-title">
			<h1> I will create a minimal business card design - <?php echo $gig_id; ?></h1>
			<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>  <b>(20 Reviews)</b></span>
		</div>
		<div class="breadcum">
			<a href="">Graphics & Design </a> / <a href="">Business Cards & Stationery </a>
		</div>

		<div class="product-slider">
			  <div id="carousel" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item active"> <img class="img-responsive img-thumbnail" src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"> </div>
						<div class="item"> <img class="img-responsive img-thumbnail"  src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"> </div>
						<div class="item"> <img class="img-responsive img-thumbnail"  src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"> </div>
						<div class="item"> <img class="img-responsive img-thumbnail"  src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"> </div>
				  </div>
			  </div>
			<div class="clearfix">
			<div id="thumbcarousel" class="carousel slide" data-interval="false">
				<div class="carousel-inner">
					<div class="item active">
						<div data-target="#carousel" data-slide-to="0" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
						<div data-target="#carousel" data-slide-to="1" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
						<div data-target="#carousel" data-slide-to="2" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
						<div data-target="#carousel" data-slide-to="3" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
						<div data-target="#carousel" data-slide-to="4" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
					</div>
					<div class="item">
						<div data-target="#carousel" data-slide-to="5" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
						<div data-target="#carousel" data-slide-to="6" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
						<div data-target="#carousel" data-slide-to="7" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
						<div data-target="#carousel" data-slide-to="8" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
						<div data-target="#carousel" data-slide-to="9" class="thumb"><img src="https://mobirise.com/bootstrap-carousel/assets2/images/hannah-rodrigo-320734-2000x1333.jpg"></div>
					</div>
				</div>
			  <!-- /carousel-inner --> 
			  <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i> </a> </div>
				<!-- /thumbcarousel --> 
			</div>
		</div>
	</div>
	
	<div class="gig-dtails table-responsive">
        <table class="table table-bordered">
			<thead>
				<tr>
					<th colspan="4">Compare Packages</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td class="p-n-p"><span>$115</span><br> Premium</td>
					<td class="p-n-p"><span>$85 </span><br>Standard</td>
					<td class="p-n-p"><span>$70 </span><br>Basic</td>
				</tr>
				<tr>
					<td>Description</td>
					<td>
						<span>FOUR DESIGNS</span>
						<br/>
						4 concepts, double-sided design, unlimited revisions, and high resolution pdf files
						ready to print!
					</td>
					<td><span>THREE DESIGNS</span><br/>
						3 concepts, double-sided design, 3 revisions, and high resolution pdf files ready to
						print!
					</td>
					<td>
						<span>ONE DESIGN</span>
						<br/>
						a double-sided design, 2 revisions, and high resolution pdf files ready to print!
					</td>
				</tr>
				<tr>
					<td>Double-Sided</td>
					<td class="text-center">
						<i class="fa fa-check"></i>
					</td>
					<td class="text-center">
						<i class="fa fa-check"></i>
					</td>
					<td class="text-center">
						<i class="fa fa-check"></i>
					</td>
				</tr>
				<tr>
					<td>Print-Ready</td>
					<td class="text-center">
						<i class="fa fa-check"></i>
					</td>
					<td class="text-center">
						<i class="fa fa-check"></i>
					</td>
					<td class="text-center">
						<i class="fa fa-check"></i>
					</td>
				</tr>
				<tr>
					<td>Design Concepts</td>
					<td class="text-center">4</td>
					<td class="text-center">3</td>
					<td class="text-center">1</td>
				</tr>
				<tr>
					<td>Revisions</td>
					<td class="text-center">unlimited</td>
					<td class="text-center">3</td>
					<td class="text-center">2</td>
				</tr>
				<tr>
					<td>Delivery time</td>
					<td class="text-center">
						<div class="radio radio-default">
							<input type="radio" name="radio2" id="radio3" value="option1">
							<label for="radio3">
								5&nbsp;days
							</label>
						</div>
						<div class="radio radio-default">
							<input type="radio" name="radio2" id="radio4" value="option2" checked="">
							<label for="radio4">
							   7&nbsp;days
							</label>
						</div>
					</td>
					<td class="text-center">
						<div class="radio radio-default">
						<input type="radio" name="radio2" id="radio3" value="option1">
						<label for="radio3">
							5&nbsp;days
						</label>
						</div>
						<div class="radio radio-default">
							<input type="radio" name="radio2" id="radio4" value="option2" checked="">
							<label for="radio4">
							   7&nbsp;days
							</label>
						</div>
					</td>
					<td class="text-center">
						<div class="radio radio-default">
							<input type="radio" name="radio2" id="radio3" value="option1">
							<label for="radio3">
								5&nbsp;days
							</label>
						</div>
						<div class="radio radio-default">
							<input type="radio" name="radio2" id="radio4" value="option2" checked="">
							<label for="radio4">
							   7&nbsp;days
							</label>
						</div>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<button class="btn  btn-block learn_btn">Continue ( 115$ )</button>
					</td>
					<td>
						<button class="btn  btn-block learn_btn">Continue ( 85$ )</button>
					</td>
					<td>
						<button class="btn  btn-block learn_btn">Continue ( 70$ )</button>
					</td>
				</tr>
			</tbody>
		</table>
    </div>
                
	<div class="gig-dtails table-responsive aboutgig">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th colspan="4">About This Gig</th>

				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<p>
							I will design you a unique, fresh, and luxurious business card. All designs are 100% original and made to meet your needs. I am open to all sizes, shapes, papers, and orientations for your business card. Not sure what you're looking for? Let's have a conversation and figure out what will work best for you and make the design pop!
						</p>
						<p>
							<b>Please let me know :</b> your timeline, your preferred size of business card, your preferred file format, where you will be getting your cards printed, and if you have a logo you'd like to include.
						</p>
						<p><b>I will offer you :</b> unique concepts for your business card, double-sided design, high resolution files (300 dpi / cmyk) in your preferred file format, and revisions to the design.</p>
						<p>I'm looking forward to hearing from you!</p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

                <div class="gig-dtails recommends">
                    <h3> Recommended for You </h3>

                    <div class="team_area best_freelancer">
                        <div class="row">
   
							<div class="col-md-4">
								<div class="team_member gigbox">
									<img src="{{asset('front_assets/image/logo_design.png')}}" alt="member">
									<div class="content text-left">
										<img class="img-circle img-thumbnail" src="{{asset('front_assets/image/logo_design.png')}}" alt="test">
										<div class="seller-info">
											<span>weperfectionist</span>
											<p class="orange">Top Rated Seller</p>
										</div>
										<div class="gig-title">
											<a href="">I will create eye catching 2d animation or cartoon explainer.</a>
										</div>
										<div class="rating-info"><i class="fa fa-star"></i> <span>4.6</span> (1k+)</div>

									</div>
									<div class="gig-footer dis-flex">
										<a href=""><i class="fa  fa-heart-o"></i></a>
										<span class="pull-right">Starting <b>$5</b></span>
									</div>
								</div>
                            </div>
							
                        </div>
                    </div>
                </div>
				
				
                <div class="gig-dtails feedback">
                    <h3> Reviews <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>  <b>(20 Reviews)</b></span></h3>

                    <div class="boxx">
                        <div class="blog-top clearfix">
                            <div class="news-allreply pull-left">
                                <a href="#"><img src="{{asset('front_assets/image/pexels-photo-1264210.jpeg')}}" class="img-circle" alt=""></a>
                            </div>
                            <div class="blog-img-details">
                                <div class="blog-title">
                                    <h4>Client Name Here<small><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 5 Star</small></h4>
                                    <span>14 October, 2016 at 6 : 00 pm</span>
                                </div>
                                <p class="p-border">Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                <div class="mt10">
                                    <div class="news-allreply pull-left">
                                        <a href="#"><img src="{{asset('front_assets/image/pexels-photo-1264210.jpeg')}}" class="img-circle" alt=""></a>
                                    </div>
                                    <div class="blog-img-details">
                                        <div class="blog-title">
                                            <h4>Seller Review <small><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 5 Star</small></h4>
                                            <span>14 October, 2016 at 6 : 00 pm</span>
                                        </div>
                                        <p class="p-border">Lorem ipsum dolor exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-top clearfix">
                            <div class="news-allreply pull-left">
                                <a href="#"><img src="{{asset('front_assets/image/pexels-photo-1264210.jpeg')}}" class="img-circle" alt=""></a>
                            </div>
                            <div class="blog-img-details">
                                <div class="blog-title">
                                    <h4>Salim Rana akter <small><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 5 Star</small></h4>
                                    <span>14 October, 2016 at 6 : 00 pm</span>
                                </div>
                                <p class="p-border">Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                <div class="mt10">
                                    <div class="news-allreply pull-left">
                                        <a href="#"><img src="{{asset('front_assets/image/pexels-photo-1264210.jpeg')}}" class="img-circle" alt=""></a>

                                    </div>
                                    <div class="blog-img-details">
                                        <div class="blog-title">
                                            <h4>Seller Review <small><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 5 Star</small></h4>
                                            <span>14 October, 2016 at 6 : 00 pm</span>
                                        </div>
                                        <p class="p-border">Lorem ipsum dolor exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-top clearfix">
                            <div class="news-allreply pull-left">
                                <a href="#"><img src="{{asset('front_assets/image/pexels-photo-1264210.jpeg')}}" class="img-circle" alt=""></a>
                            </div>
                            <div class="blog-img-details">
                                <div class="blog-title">
                                    <h4>Salim Rana akter <small><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 5 Star</small></h4>
                                    <span>14 October, 2016 at 6 : 00 pm</span>
                                </div>
                                <p class="p-border">Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                <div class="mt10">
                                    <div class="news-allreply pull-left">
                                        <a href="#"><img src="{{asset('front_assets/image/pexels-photo-1264210.jpeg')}}" class="img-circle" alt=""></a>
                                    </div>
                                    <div class="blog-img-details">
                                        <div class="blog-title">
                                            <h4>Seller Review <small><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 5 Star</small></h4>
                                            <span>14 October, 2016 at 6 : 00 pm</span>
                                        </div>
                                        <p class="p-border">Lorem ipsum dolor exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-top clearfix">
                            <div class="news-allreply pull-left">
                                <a href="#"><img src="{{asset('front_assets/image/pexels-photo-1264210.jpeg')}}" class="img-circle" alt=""></a>
                            </div>
                            <div class="blog-img-details">
                                <div class="blog-title">
                                    <h4>Salim Rana akter <small><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 5 Star</small></h4>
                                    <span>14 October, 2016 at 6 : 00 pm</span>
                                </div>
                                <p class="p-border">Lorem ipsum dolor sit amet, consectetur adipisicing elit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                <div class="mt10">
                                    <div class="news-allreply pull-left">
                                        <a href="#"><img src="{{asset('front_assets/image/pexels-photo-1264210.jpeg')}}" class="img-circle" alt=""></a>
                                    </div>
                                    <div class="blog-img-details">
                                        <div class="blog-title">
                                            <h4>Seller Review <small><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 5 Star</small></h4>
                                            <span>14 October, 2016 at 6 : 00 pm</span>
                                        </div>
                                        <p class="p-border">Lorem ipsum dolor exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</section>


@endsection	