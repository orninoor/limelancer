@extends('admin/master')
@section('main_content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb float-xl-right">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;">Page Options</a></li>
            <li class="breadcrumb-item active">Page with Top Menu</li>
        </ol>
        <!-- end breadcrumb -->

        <!-- begin page-header -->
        <h1 class="page-header">Manage {{ $heading }} <small>{{ $Sub_heading }}</small></h1>
        <!-- end page-header -->
        <!-- begin panel -->

        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Panel Title here</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                </div>
            </div>
            <div class="panel-body">

                <form class="form-horizontal" action="{{URL::to($action)}}" role="form" method="POST" id="form" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group row m-b-6">
                        <label class="col-md-2 col-form-label">Category Name</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="name" value="{{ $editdata->name }}" required/>
                        </div>
                    </div>

                    <div class="form-group row m-b-6">
                        <label class="col-md-2 col-form-label">Select Parent</label>
                        <div class="col-md-2">
                            <select name="parent_id" class="form-control">
                                <option value="0">Select Parent Category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{ $category->id === $editdata->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                    @if ($category->children)
                                        @foreach ($category->children as $child)
                                            <option value="{{ $child->id }}" {{ $child->id === old('category_id') ? 'selected' : '' }}>&nbsp;&nbsp;{{ $child->name }}</option>
                                        @endforeach
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>



                    <div class="form-group row">
                        <div class="col-md-7 offset-md-3">
                            <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
                            <button type="reset" class="btn btn-sm btn-default">Cancel</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <!-- end panel -->


        <!-- end #content -->

@stop
