<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{$title}}</title>
		<!--    favicon-->
		<link rel="shortcut icon" href="{{asset('front_assets/image/icon.png')}}" type="image/x-icon">
        <!-- Bootstrap -->
        <link href="{{asset('front_assets/css/all-plugin.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('front_assets/vendors/themify-icon/themify-icons.css')}}">
        <!--css-->
		<link href="https://fonts.googleapis.com/css?family=Coiny" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('front_assets/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('front_assets/css/responsive.css')}}"> 
		<style>
		   .carousel-inner{
			overflow: hidden !important;
		   }
		   .carousel-caption{
			text-align: right !important;
		  
		   }
			.carousel-caption h3{
				  color: #fff;
			}
			 .carousel-caption h3 small{
				font-size: 12px;
					color: #fff;
					font-weight: bold;
			 }
			 .carousel-caption p{
				font-size: 13px;
				margin-top: 10px;
			  line-height: 17px;
					color: #fff;
			}
        </style>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="70">
    
        <!--start header Area-->
        <nav class="navbar navbar-fixed-top header_five" data-spy="affix" data-offset-top="70">
            <div class="col-md-12">
                <!--========== Brand and toggle get grouped for better mobile display ==========-->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="lnr lnr-menu"></i>
                        <i class="lnr lnr-cross"></i>
                    </button>
                    <a class="navbar-brand logo-main" href="{{URL::to('/')}}">Lancer
                        <!--<img src="{{asset('front_assets/image/new-logo4.png')}}" alt="logo">
                        <img src="{{asset('front_assets/image/new-logo3.png')}}" alt="logo">-->
                    </a>
                </div>
                <a class="banner_btn btn-getnow hidden-sm hidden-xs" href="#" data-toggle="modal" data-target="#myModal">Join Now</a>
                <!--========== Collect the nav links, forms, and other content for toggling ==========-->
                <div class="collapse navbar-right navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav menu" id="nav">
						<li><a href="{{URL::to('/')}}/">Home</a></li>
						<li><a href="{{URL::to('/')}}/gigs">Gig</a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal">Sign In</a></li>
						<li class="dropdown msg ">
                            <a href="#" id="message" >Messages</a>
                            <ul class="dropdown-menu message-panel">
                                <ul class="nav nav-tabs two-opt">
                                    <li class="notific"><span><i class="fa fa-microphone"></i> Notifications (1)</span></li>
                                    <li class="inbox"><span><i class="fa fa-comment-o"></i> Inbox (5)</span></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="notific-nicescrol">
										<div id="notific" class="tab-pane active-me">
											<div class="single-notific new">
												<div class="pull-left"><img src="https://via.placeholder.com/45" class="img-circle img-thumbnail"></div>
												<div class="pull-left content-sumary">
													<div class="user-info"><b>Md MOnirul Islam</b> Follow  the ebook and 2019 could  be  your  most successful  year
														<small class="clearfix">1 Day ago</small>
													</div>
													<div class="icon-opt"><a href="" title="Mark as Unread"><i class="fa fa-envelope-o pull-right"></i></a></div>
												</div>
											</div><!--single end-->
											
											<div class="single-notific">
												<div class="pull-left"><img src="https://via.placeholder.com/45" class="img-circle img-thumbnail"></div>
												<div class="pull-left content-sumary">
													<div class="user-info"><b>Zebclark</b> Left a 5 star <a href="">Review</a>
														<small class="clearfix">10 Day ago</small>
													</div>
													<div class="icon-opt">
														<a href="" title="Mark as Unread"><i class="fa fa-envelope-o pull-right"></i></a>
														<div class="gigpic">
															   <img src="{{asset('front_assets/image/gigsm.png')}}" class="img-responsive" />
														</div>
													</div>
												</div>
											</div><!--single end-->
										</div>
									
                                        <div id="inbox" class="tab-pane">
                                            <div class="single-notific new">
                                                <div class="pull-left"><img src="https://via.placeholder.com/45" class="img-circle img-thumbnail"></div>
                                                <div class="pull-left content-sumary">
                                                    <div class="user-info"><b>Md MOnirul Islam</b> Follow  the ebook and 2019 could  be  your  most successful  year
                                                        <small class="clearfix">1 Day ago</small>
                                                    </div>
                                                    <div class="icon-opt"><a href="" title="Mark as Unread"><i class="fa fa-envelope-o pull-right"></i></a></div>
                                                </div>
                                            </div><!--single end-->
                                        </div>
                                    </div>
									<div class="set-option">
										<a href="" title="Enable Sount"><i class="fa fa-volume-up" aria-hidden="true"></i></a>
										<a href="" title="Change Settings"><i class="fa fa-cog" aria-hidden="true"></i></a>
									</div>
                                </div>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <!--End header Area-->
        <!--Main Slider-->
        <section id="home" class="header-home-five home-six">
            <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="none" data-interval="5000" data-scroll-index="0">
                <!-- Wrapper For Slides -->
                <div class="carousel-inner" role="listbox">
                    <!-- Third Slide -->
                    <div class="item active">
                        <!-- Slide Background -->
                          <img src="{{asset('front_assets/image/hands-people-woman-working.jpg')}}"  alt="Bootstrap Touch Slider" class="slide-image" />
                        <div class="bs-slider-overlay"></div>
                    </div>
                      
                    <!-- End of Slide -->
                    <!-- Second Slide -->
                    
                    <!-- End of Slide -->
                    <!-- Third Slide -->
                   
                    <!-- End of Slide -->
                </div>
                <!-- End of Wrapper For Slides -->
                <div class="slider_content">
                    <div class="container">
                        <div class="row" style="height: 100vh; ">
                            <div class="col-sm-7 col-header-text lr-padding" style="padding-top: 240px;">
                                <h1>Find The Perfect  <br/>Services For Your  Business</h1>
                                <br />
								<div>
									<form class="mailchimp" method="post" novalidate="">
										<div class="input-group  subcribes custom-width">
											<input type="email" name="EMAIL" class="form-control memail" placeholder="Type Anything to search...">
											<span class="input-group-btn">
												<button class="btn btn-submit base-color custom-mg" type="submit">Search </button>
											</span>
										</div>
										<br />
										<div >
										<span class="btn-link btn btn-sm" style="color: #fff;font-size: 14px;">Popular Tags</span>
										<span class="btn-default btn btn-sm">Video</span>
										<span class="btn-default btn btn-sm">Graphics</span>
										<span class="btn-default btn btn-sm">Logo</span>
										</div>
										
									</form>
								</div>
                            </div>
                            <div class="col-md-5  right-padding" style="height: 100vh; ">
                               <div   class="carousel fade mkau"   data-ride="carousel" style="vertical-align: bottom;">
									<div class="carousel-inner">
										<div class="item active">
											<img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/matthew.029a567.png" alt="Chania">
											<div class="carousel-caption">
												<h3>Martin <br /><small>Owner Of freelace company</small></h3>
												<p><i class="fa fa-quote-left" style="color: #FF9100;" aria-hidden="true"></i> The atmosphere in Chania has a touch of Florence and Venice. <i class="fa fa-quote-right" style="color: #FF9100;"  aria-hidden="true"></i></p>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
 
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog"  >
		<div class="modal-dialog lo-sign-custom">
			<div class="modal-content">
				<div class="modal-body">
					<div class="text-center m-logo mb5">Lancer</div>
					<form class="no-ovflow">
						<div class="col-md-12">
							<div class="form-group">
								<label for="email1">Email address</label>
								<input type="email" class="form-control" id="email1" >
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="username">Username</label>
								<input type="text" class="form-control" id="username">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="Password">Password</label>
								<input type="password" class="form-control" id="Password">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="cPassword">Confirm Password</label>
								<input type="password" class="form-control" id="cPassword">
							</div>
						</div>
						<div class="col-md-12">
							<div class="checkbox">
								<input id="checkbox1" type="checkbox">
								<label for="checkbox1">
									I agree to the <a href="">Terms and Conditions.</a> 
								</label>
							</div>
							<div>
								<button class="btn  btn-base btn-sm mt5">Create Account</button>
							</div>
							<div class="orlogin text-center">
								<span>OR</span>		 
							</div>
							<div class="text-center top-mg">
								<a href="" class="twitter-btn"><i class="fa fa-twitter"></i> &nbsp;Twitter</a>
								<a href="" class="fb-button"><i class="fa fa-facebook"></i> &nbsp;Facebook</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
  
	<!--End Main Slider-->
	<!--=========Start partner_logo ===========-->
	<!-- <section class="partner_logo_area">
		<div class="container text-center logo-list">
			<img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/netflix.4d7adbc.png" alt="">
			<img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/png.8a4a831.png" alt="">
			<img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/facebook.268fb97.png" alt="">
			<img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/ny.4f3c7ee.png" alt=""> <img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/netflix.4d7adbc.png" alt="">
			<img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/png.8a4a831.png" alt="">
			<img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/facebook.268fb97.png" alt="">
		</div>
	</section>-->
	<!--=========End partner_logo ===========-->
		
        <!--Start overview area -->
        <section class="overview_area_70" id="features">
            <div class="container">
            <div class="sec_title_five sec_five">
                <h2>Work Categroy</h2>
                <div class="br"></div>
            </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="overview_item">
                            <img src="{{asset('front_assets/icon/idea.png')}}" alt="" height="50px" />
                            <h2 class="title">Android</h2>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="overview_item">
                          <img src="{{asset('front_assets/icon/android.png')}}" alt="" height="50px" />
                            <h2 class="title">Web Research</h2>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="overview_item">
                          <img src="{{asset('front_assets/icon/website-design-symbol.png')}}" alt="" height="50px" />
                            <h2 class="title">Web Designing</h2>
                            
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="overview_item">
                           <img src="{{asset('front_assets/icon/html5.png')}}" alt="" height="50px" />
                            <h2 class="title">Html Design</h2>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="overview_item">
                          <img src="{{asset('front_assets/icon/wp.svg')}}" alt="" height="50px" />
                            <h2 class="title">Wordpress</h2>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="overview_item">
                           <img src="{{asset('front_assets/icon/html5.png')}}" alt="" height="50px" />
                            <h2 class="title">Html Design</h2>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="overview_item">
                          <img src="{{asset('front_assets/icon/apple.png')}}" alt="" height="50px" />
                            <h2 class="title">App Development</h2>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="overview_item">
                            <img src="{{asset('front_assets/icon/idea.png')}}" alt="" height="50px" />
                            <h2 class="title">Android</h2>  
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="overview_item">
                          <img src="{{asset('front_assets/icon/android.png')}}" alt="" height="50px" />
                            <h2 class="title">Web Research</h2>
                        </div>
                    </div>
                     <div class="col-sm-2">
                        <div class="overview_item">
                          <img src="{{asset('front_assets/icon/apple.png')}}" alt="" height="50px" />
                            <h2 class="title">App Development</h2>
                        </div>
                    </div>   
                    <div class="col-sm-2">
                        <div class="overview_item">
                          <img src="{{asset('front_assets/icon/website-design-symbol.png')}}" alt="" height="50px" />
                            <h2 class="title">Web Designing</h2>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="overview_item">
                          <img src="{{asset('front_assets/icon/wp.svg')}}" alt="" height="50px" />
                            <h2 class="title">Wordpress</h2>
                        </div>
                    </div>
                </div> 
                <br /><br /><br />
            </div>
        </section>
        <!--End overview area -->
	<section style="background: #f3f3f3; padding-bottom: 43px;">  
		<div class="container">
			<div class="cl_slider">
                <div class="clients-lg-slider owl-carousel">
                    <div class="item">
                       <a href="#"><img src="{{asset('front_assets/image/clients-logo/logo5.png')}}" alt=""></a>
                    </div>
                    <div class="item">
                       <a href="#"><img src="{{asset('front_assets/image/clients-logo/logo6.png')}}" alt=""></a>
                    </div>
                    
                    <div class="item">
                       <a href="#"><img src="{{asset('front_assets/image/clients-logo/logo8.png')}}" alt=""></a>
                    </div>
                    <div class="item">
                       <a href="#"><img src="{{asset('front_assets/image/clients-logo/logo5.png')}}" alt=""></a>
                    </div>
                </div>
			</div>
        </div>   
	</section>
         
	<section class="team_area best_freelancer" id="team" style="margin-bottom: 29px !important;">
		<div class="container">
			<div class="sec_title_five sec_five">
				<div><h2>Popular Gigs</h2></div> <a href="" class="pull-right banner_btn btn-getnow  " style="margin-top: -62px !important;">View All</a>
				<div class="br"></div>        
			</div>
			<span class="clearfix"></span>
			<div class="row">
			   <div class="col-md-3">
				   <div class="team_member gigbox">
						<img src="{{asset('front_assets/image/logo_design.png')}}" alt="member">
						<div class="content text-left">
							<img class="img-circle img-thumbnail" src="https://via.placeholder.com/40" alt="test">
							<div class="seller-info"> 
								<span>weperfectionist</span>
								<p class="orange">Top Rated Seller</p>
							</div>
						</div>
					</div>
				</div>
				   
				<div class="col-md-3">
					<div class="team_member gigbox">
						<img src="{{asset('front_assets/image/logo_design.png')}}" alt="member">
						<div class="content text-left">
							 <img class="img-circle img-thumbnail" src="https://via.placeholder.com/40" alt="test">
							 <div class="seller-info"> 
							  <span>weperfectionist</span>
								<p class="orange">Top Rated Seller</p>
							 </div>

						</div>
					</div>
				</div>
			</div>      
		</div>
    </section>
        
	<section class="features_area_six features_area_seven" id="features" style="background: #f2f2f2;">
		<div class="container">
			<div class="row">
				<div class="col-sm-7 f_img text-left">
					<img class="features_img_first" src="{{asset('front_assets/image/desktop_abc.png')}}" alt="featured">
				</div>
				<div class="col-sm-5  features_content_two" style="padding-top: 0 !important;">
					<div class="sec_title_five sec_five">
					<span>polash Discover Freelancers</span>
						<h2>Hire in minutes.Pay per hour.</h2>
						<div class="br"></div>
					</div>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<a href="#" class="btn learn_btn">Browse Freelancers</a>
				</div>
			</div>
		</div>
    </section>
	<section class="features_area_six features_area_seven" id="features">
		<div class="container">
			<div class="row">
			  <div class="col-sm-5  features_content_two">
					<div class="sec_title_five sec_five">
					<span>Let freelancers discover you</span>
						<h2>Post projects quickly,receive responses even quicker.</h2>
						<div class="br"></div>
					</div>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<a href="#" class="btn learn_btn">Browse Jobs</a>
				</div>
				<div class="col-sm-7 f_img text-right">
					<img class="features_img_fist" src="{{asset('front_assets/image/home-6/phone-m3.png')}}" alt="featured">
					<img class="features_img" src="{{asset('front_assets/image/home-6/app-screen.png')}}" alt="">
				</div>
			  
			</div>
		</div>
    </section>
    
	<!--team area-->
        <section class="team_area guide_to_work" id="team">
            <div class="container">
                <div class="section_title color_w">
                    <h2>Guide to work with Us</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipiscing elit donec tempus pellentesque dui vel tristique purus justo vestibulum eget lectus non gravida ultrices</p>
                </div>
                <div class="row m0 team-carousel owl-carousel">
                    <div class="team_member">
                        <img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/1440-create-website.0fe083f.jpg" alt="member">
                        <div class="content text-left">
                            <h2>Create a Website</h2>
                            <p>Nullam dictum sapien vitae lorem ultr varius. Nulla volutpat nisl augue Proin vehicula mauris.</p>
                        </div>
                    </div>
                    <div class="team_member">
                        <img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/1440-digital-marketing.46ef134.jpg" alt="member">
                        <div class="content">
                            <h2>Grow With Digital Marketing</h2>
                            <p>Nullam dictum sapien vitae lorem ultr varius. Nulla volutpat nisl augue Proin vehicula mauris.</p>
                        </div>
                    </div>
                    <div class="team_member">
                        <img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/1440-create-website.0fe083f.jpg" alt="member">
                        <div class="content text-left">
                            <h2>Create a Website</h2>
                            <p>Nullam dictum sapien vitae lorem ultr varius. Nulla volutpat nisl augue Proin vehicula mauris.</p>

                        </div>
                    </div>
                    <div class="team_member">
                        <img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/1440-digital-marketing.46ef134.jpg" alt="member">
                        <div class="content">
                            <h2>Grow With Digital Marketing</h2>
                            <p>Nullam dictum sapien vitae lorem ultr varius. Nulla volutpat nisl augue Proin vehicula mauris.</p>
                        </div>
                    </div>
                    <div class="team_member">
                        <img src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/1440-strong-brand.64effa5.jpg" alt="member">
                        <div class="content">
                            <h2>Build a Strong Brand</h2>
                            <p>Nullam dictum sapien vitae lorem ultr varius. Nulla volutpat nisl augue Proin vehicula mauris.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!--testimonial_area-->
        <section class="testimonial_area_two testimonial_area_six" id="testimonial">
            <div class="container">
                <div class="sec_title_five text-center">
                    <h2>Best Client's Review</h2>
                    <div class="br"></div>
                </div>
                <div class="row">
                    <div id="test_c_six" class="testimonial_carousel_two owl-carousel">
                        <div class="item">
                            <div class="testimonial_item">
                                <div class="icon">
                                    <i class="fa fa-quote-right"></i>
                                </div>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus.</p>
                                <div class="media">
                                    <div class="media-left">
                                        <img class="img-circle" src="{{asset('front_assets/image/689-85x85.jpg')}}" alt="test">
                                    </div>
                                    <div class="media-body">
                                        <h2>John Wilson</h2>
                                        <h6>Director @ LenrikMedia</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial_item">
                                <div class="icon">
                                    <i class="fa fa-quote-right"></i>
                                </div>
                                <p>polash Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus.</p>
                                <div class="media">
                                    <div class="media-left">
                                        <img class="img-circle" src="{{asset('front_assets/image/689-85x85.jpg')}}" alt="test">
                                    </div>
                                    <div class="media-body">
                                        <h2>Sayedullah Kabed</h2>
                                        <h6>CEO & Co-founder</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial_item">
                                <div class="icon">
                                    <i class="fa fa-quote-right"></i>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                <div class="media">
                                    <div class="media-left">
                                        <img class="img-circle" src="{{asset('front_assets/image/689-85x85.jpg')}}" alt="test">
                                    </div>
                                    <div class="media-body">
                                        <h2>Toni Roberts</h2>
                                        <h6>Director @ LenrikMedia</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--testimonial_area-->
 
        <!-- start faq area -->
        <section class="faq-area-2" id="faq">
            <div class="container">
                <div class="sec_title_five text-center">
                    <h2>Faq</h2>
                    <div class="br"></div>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut <br>fugit, sed consequuntur magni dolores ratione voluptatem sequi nesciunt.</p>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-7">
                        <div class="panel-group faq-inner-accordion faq_accordian_two" id="accordion" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="btn-accordion" aria-expanded="true" role="button">
                                            <i class="ti-plus plus"></i><i class="ti-minus minus"></i>Aspernatur remaining essentially unchanged?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in" aria-expanded="true" role="tabpanel">
                                    <div class="panel-body">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="btn-accordion collapsed" aria-expanded="false">
                                            <i class="ti-plus plus"></i><i class="ti-minus minus"></i>Voluptatem quia voluptas sit aspernatur?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" role="tabpanel">
                                    <div class="panel-body">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="btn-accordion collapsed" aria-expanded="false">
                                            <i class="ti-plus plus"></i><i class="ti-minus minus"></i>Combined with a handful sentence structures?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" role="tabpanel">
                                    <div class="panel-body">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="btn-accordion collapsed" aria-expanded="false">
                                            <i class="ti-plus plus"></i><i class="ti-minus minus"></i>Many desktop publishing web page?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" role="tabpanel">
                                    <div class="panel-body">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-5">
                        <div class="faq-img">
                            <img class="img-responsive" src="https://npm-assets.fiverrcdn.com/assets/@fiverr/logged_out_homepage_perseus/apps/ipadX1.810cb55.gif" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End faq area -->





        <footer class="row footer-area footer_five">
            <div class="footer-top">
                <div class="container">
                    <div class="row footer_sidebar">
                        <div class="widget widget1 about_us_widget col-xs-6 col-sm-6 col-md-3 wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms" data-wow-offset="0" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeIn;">
                            <a href="index.html" class="logo logo-main flogo " style="color: #fff !important;">
                           Lancer
                            </a>
                            <p>Lorem ipsum dolor sit consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud.</p>
                            <ul class="nav social_icon row m0">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            </ul>
                        </div>
                        <div class="widget widget2 widget_contact col-xs-6 col-sm-6 col-md-3 wow fadeIn" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeIn;">
                            <h4 class="widget_title">Categories</h4>
                            <div class="widget_inner row m0">
                                <ul>
                                    <li><a href="">Graphics & Design</a></li>
                                    <li><a href="">Digital Marketing</a></li>
                                    <li><a href="">Writing & Translation</a></li>
                                    <li><a href="">Video & Animation</a></li>
                                    <li><a href="">Music & Audio</a></li>
                                    <li><a href="">Programming & Tech</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                        
                        <div class="widget widget3 widget_twitter  col-xs-6 col-sm-6 col-md-3 wow fadeIn" data-wow-delay="150ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 150ms; animation-name: fadeIn;">
                            <h4 class="widget_title">About</h4>
                            <div class="widget_inner row m0">
                                <ul class="tweets">
                                    <li><a href="">Careers</a></li>
                                    <li><a href="">Press & News</a></li>
                                    <li><a href="">Partnerships</a></li>
                                    <li><a href="">Privacy Policy</a></li>
                                    <li><a href="">Terms of Service</a></li>
                                    <li><a href="">Intellectual Property Claims</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget widget4 widget_instagram  col-xs-6 col-sm-6 col-md-3 wow fadeIn" data-wow-delay="200ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 200ms; animation-name: fadeIn;">
                            <h4 class="widget_title">Top Community</h4>
                            <div class="widget_inner row m0">
                                <ul class="tweets">
                                     <li><a href="">Careers</a></li>
                                    <li><a href="">Press & News</a></li>
                                    <li><a href="">Partnerships</a></li>
                                    <li><a href="">Privacy Policy</a></li>
                                    <li><a href="">Terms of Service</a></li>
                                    <li><a href="">Intellectual Property Claims</a></li>
                                </ul>
                                <span>Follow us on <a href="#">@instagram</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m0 footer_bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-5">  
                            © 2018 All Right Reserved | <a href="index.html">Exitolabs</a>
                        </div>
                        <div class="right col-sm-7">
                            <ul class="footer-menu">
                                <li><a href="#">About</a></li>
                                <li><a href="#">Help</a></li>
                                <li><a href="#"> Contact</a></li>
                                <li><a href="#">Terms</a></li>
                                <li><a href="#">Privacy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        
		<script type="text/javascript" src="{{asset('front_assets/js/jquery-2.2.4.js')}}"></script>
        <script type="text/javascript" src="{{asset('front_assets/js/bootstrap.min.js')}}"></script>
        <!-- waypoints js-->
        <script src="{{asset('front_assets/vendors/waypoints/waypoints.min.js')}}"></script>
        <script src="{{asset('front_assets/vendors/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('front_assets/vendors/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('front_assets/vendors/swipper/swiper.min.js')}}"></script>
        <script src="{{asset('front_assets/vendors/parallax/twinlight.js')}}"></script>
        <script src="{{asset('front_assets/vendors/parallax/jquery.wavify.js')}}"></script>
        <script src="{{asset('front_assets/js/nav.js')}}"></script>
        <!--owl carousel js-->
        <script type="text/javascript" src="{{asset('front_assets/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
        <!--custom js -->
        <script type="text/javascript" src="{{asset('front_assets/js/custom.js')}}"></script>
     
    </body>
</html>