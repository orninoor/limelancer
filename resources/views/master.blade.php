<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ $title}} </title>
		<!--    favicon-->
		<link rel="shortcut icon" href="{{asset('front_assets/image/icon.png')}}" type="image/x-icon">
        <!-- Bootstrap -->
        <link href="{{asset('front_assets/css/all-plugin.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('front_assets/vendors/themify-icon/themify-icons.css')}}">
        <!--css-->
		<link href="https://fonts.googleapis.com/css?family=Coiny" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('front_assets/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('front_assets/css/responsive.css')}}">
        <style>
		.carousel-inner{
			overflow: hidden !important;
		}
		.carousel-caption{
			text-align: right !important;
      
		}
        .carousel-caption h3{
            color: #fff;
        }
        .carousel-caption h3 small{
            font-size: 12px;
                color: #fff;
                font-weight: bold;
        }
        .carousel-caption p{
            font-size: 13px;
            margin-top: 10px;
			line-height: 17px;
            color: #fff;
        }
        </style>
    </head>
	
	
    <body data-spy="scroll" data-target=".navbar" data-offset="70">
    
        <!--start header Area-->
        <nav class="navbar  header_five static-menu">
            <div class="col-md-12">
                <!--========== Brand and toggle get grouped for better mobile display ==========-->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="lnr lnr-menu"></i>
                        <i class="lnr lnr-cross"></i>
                    </button>
                    <a class="navbar-brand logo-main" href="{{URL::to('/')}}">Lancer
                        <!--<img src="{{asset('front_assets/image/new-logo4.png')}}" alt="logo">
                        <img src="{{asset('front_assets/image/new-logo3.png')}}" alt="logo">-->
                    </a>
                </div>
                <a class="banner_btn btn-getnow hidden-sm hidden-xs" href="#" data-toggle="modal" data-target="#myModal">Join Now</a>
                <!--========== Collect the nav links, forms, and other content for toggling ==========-->
                <div class="collapse navbar-right navbar-collapse" id="bs-example-navbar-collapse-1">
                    
			
						
					<ul class="nav navbar-nav menu" id="nav">
						<li><a href="{{URL::to('/')}}/">Home</a></li>
						<li><a href="{{URL::to('/')}}/gigs">Gig</a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal">Sign In</a></li>
                        <li class="dropdown msg ">
                            <a href="#" id="message" >Messages</a>
                            <ul class="dropdown-menu message-panel">
                                <ul class="nav nav-tabs two-opt">
                                    <li class="notific"><span><i class="fa fa-microphone"></i> Notifications (1)</span></li>
                                    <li class="inbox"><span><i class="fa fa-comment-o"></i> Inbox (5)</span></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="notific-nicescrol">
										<div id="notific" class="tab-pane active-me">
											<div class="single-notific new">
												<div class="pull-left"><img src="https://via.placeholder.com/45" class="img-circle img-thumbnail"></div>
												<div class="pull-left content-sumary">
													<div class="user-info"><b>Md MOnirul Islam</b> Follow  the ebook and 2019 could  be  your  most successful  year
														<small class="clearfix">1 Day ago</small>
													</div>
													<div class="icon-opt"><a href="" title="Mark as Unread"><i class="fa fa-envelope-o pull-right"></i></a></div>
												</div>
											</div><!--single end-->
											
											<div class="single-notific">
												<div class="pull-left"><img src="https://via.placeholder.com/45" class="img-circle img-thumbnail"></div>
												<div class="pull-left content-sumary">
													<div class="user-info"><b>Zebclark</b> Left a 5 star <a href="">Review</a>
														<small class="clearfix">10 Day ago</small>
													</div>
													<div class="icon-opt">
														<a href="" title="Mark as Unread"><i class="fa fa-envelope-o pull-right"></i></a>
														<div class="gigpic">
															   <img src="{{asset('front_assets/image/gigsm.png')}}" class="img-responsive" />
														</div>
													</div>
												</div>
											</div><!--single end-->
										</div>
									
                                        <div id="inbox" class="tab-pane">
                                            <div class="single-notific new">
                                                <div class="pull-left"><img src="https://via.placeholder.com/45" class="img-circle img-thumbnail"></div>
                                                <div class="pull-left content-sumary">
                                                    <div class="user-info"><b>Md MOnirul Islam</b> Follow  the ebook and 2019 could  be  your  most successful  year
                                                        <small class="clearfix">1 Day ago</small>
                                                    </div>
                                                    <div class="icon-opt"><a href="" title="Mark as Unread"><i class="fa fa-envelope-o pull-right"></i></a></div>
                                                </div>
                                            </div><!--single end-->
                                        </div>
                                    </div>
									<div class="set-option">
										<a href="" title="Enable Sount"><i class="fa fa-volume-up" aria-hidden="true"></i></a>
										<a href="" title="Change Settings"><i class="fa fa-cog" aria-hidden="true"></i></a>
									</div>
                                </div>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <!--End header Area-->
		
		
		
		
		
		
		
		<!-- Start Dynamic Content -->
		@yield('main_content')
		<!-- end Dynamic Content -->
		
		
		<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog"  >
		<div class="modal-dialog lo-sign-custom">
			<div class="modal-content">
				<div class="modal-body">
					<div class="text-center m-logo mb5">Lancer</div>
					<form class="no-ovflow">
						<div class="col-md-12">
							<div class="form-group">
								<label for="email1">Email address</label>
								<input type="email" class="form-control" id="email1" >
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="username">Username</label>
								<input type="text" class="form-control" id="username">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="Password">Password</label>
								<input type="password" class="form-control" id="Password">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="cPassword">Confirm Password</label>
								<input type="password" class="form-control" id="cPassword">
							</div>
						</div>
						<div class="col-md-12">
							<div class="checkbox">
								<input id="checkbox1" type="checkbox">
								<label for="checkbox1">
									I agree to the <a href="">Terms and Conditions.</a> 
								</label>
							</div>
							<div>
								<button class="btn  btn-base btn-sm mt5">Create Account</button>
							</div>
							<div class="orlogin text-center">
								<span>OR</span>		 
							</div>
							<div class="text-center top-mg">
								<a href="" class="twitter-btn"><i class="fa fa-twitter"></i> &nbsp;Twitter</a>
								<a href="" class="fb-button"><i class="fa fa-facebook"></i> &nbsp;Facebook</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
  
		


        <footer class="row footer-area footer_five">
            <div class="footer-top">
                <div class="container">
                    <div class="row footer_sidebar">
                        <div class="widget widget1 about_us_widget col-xs-6 col-sm-6 col-md-3 wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms" data-wow-offset="0" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeIn;">
                            <a href="index.html" class="logo logo-main flogo " style="color: #fff !important;">
                           Lancer
                            </a>
                            <p>Lorem ipsum dolor sit consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud.</p>
                            <ul class="nav social_icon row m0">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            </ul>
                        </div>
                        <div class="widget widget2 widget_contact col-xs-6 col-sm-6 col-md-3 wow fadeIn" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeIn;">
                            <h4 class="widget_title">Categories</h4>
                            <div class="widget_inner row m0">
                                <ul>
                                    <li><a href="">Graphics & Design</a></li>
                                    <li><a href="">Digital Marketing</a></li>
                                    <li><a href="">Writing & Translation</a></li>
                                    <li><a href="">Video & Animation</a></li>
                                    <li><a href="">Music & Audio</a></li>
                                    <li><a href="">Programming & Tech</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                        
                        <div class="widget widget3 widget_twitter  col-xs-6 col-sm-6 col-md-3 wow fadeIn" data-wow-delay="150ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 150ms; animation-name: fadeIn;">
                            <h4 class="widget_title">About</h4>
                            <div class="widget_inner row m0">
                                <ul class="tweets">
                                    <li><a href="">Careers</a></li>
                                    <li><a href="">Press & News</a></li>
                                    <li><a href="">Partnerships</a></li>
                                    <li><a href="">Privacy Policy</a></li>
                                    <li><a href="">Terms of Service</a></li>
                                    <li><a href="">Intellectual Property Claims</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget widget4 widget_instagram  col-xs-6 col-sm-6 col-md-3 wow fadeIn" data-wow-delay="200ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 200ms; animation-name: fadeIn;">
                            <h4 class="widget_title">Top Community</h4>
                            <div class="widget_inner row m0">
                                <ul class="tweets">
                                     <li><a href="">Careers</a></li>
                                    <li><a href="">Press & News</a></li>
                                    <li><a href="">Partnerships</a></li>
                                    <li><a href="">Privacy Policy</a></li>
                                    <li><a href="">Terms of Service</a></li>
                                    <li><a href="">Intellectual Property Claims</a></li>
                                </ul>
                                <span>Follow us on <a href="#">@instagram</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m0 footer_bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-5">  
                            © <?php echo date('Y');?> All Right Reserved | <a href="index.html">Exitolabs</a>
                        </div>
                        <div class="right col-sm-7">
                            <ul class="footer-menu">
                                <li><a href="#">About</a></li>
                                <li><a href="#">Help</a></li>
                                <li><a href="#"> Contact</a></li>
                                <li><a href="#">Terms</a></li>
                                <li><a href="#">Privacy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>


		
		<script type="text/javascript" src="{{asset('front_assets/js/jquery-2.2.4.js')}}"></script>
        <script type="text/javascript" src="{{asset('front_assets/js/bootstrap.min.js')}}"></script>
        <!-- waypoints js-->
        <script src="{{asset('front_assets/vendors/waypoints/waypoints.min.js')}}"></script>
        <script src="{{asset('front_assets/vendors/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('front_assets/vendors/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('front_assets/vendors/swipper/swiper.min.js')}}"></script>
        <script src="{{asset('front_assets/vendors/parallax/twinlight.js')}}"></script>
        <script src="{{asset('front_assets/vendors/parallax/jquery.wavify.js')}}"></script>
        <script src="{{asset('front_assets/js/nav.js')}}"></script>
        <!--owl carousel js-->
        <script type="text/javascript" src="{{asset('front_assets/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
        <!--custom js -->
        <script type="text/javascript" src="{{asset('front_assets/js/custom.js')}}"></script>
		
		
    </body>
</html>