<?php


Route::GET('/gig_detail/{gid_id}','WebController@gig_details' );
$segment = request()->segment(1);
$controllar = ucfirst($segment);
// Dynamic Routes
Route::resource('/'.$segment,$controllar.'Controller');
// END Dynamic Routes


Route::GET('/','WebController@index');
Route::GET('/gigs','WebController@gigs');


//For Admin Login Controller
Route::GET('/admin','LoginController@index');
Route::POST('/admin-login-check','LoginController@adminLoginCheck' );
Route::GET('/logout','DashboardController@logout' );
Route::GET('/dashboard','DashboardController@index' );
// End Login Controller

// AJAX LIST API
Route::POST('/all-data-list','ListController@all_data');
Route::POST('/all-data-user_type','User_typeController@all_data');
Route::POST('/all-data-user_roles','User_rolesController@all_data');
Route::POST('/all-data-division','DivisionController@all_data');
Route::POST('/all-data-district','DistrictController@all_data');
Route::POST('/all-data-upazila','UpazilaController@all_data');
Route::POST('/all-data-users','UsersController@all_data');
Route::POST('/all-data-nav_menu','Nav_menuController@all_data');
Route::POST('/all-data-menu','MenuController@all_data');
Route::POST('/all-data-sub_menu','sub_menuController@all_data');




