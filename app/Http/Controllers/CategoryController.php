<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    public function index(){
        $data = array();
        $data['heading'] 		= 'Manage Category ';
        $data['Sub_heading'] 	= 'Category Management';
        $data['page_type'] 		= 0;

        $data['categories']     = Category::with('children')->get();
        return view('admin/category/index', $data);
    }

    public function create(){
        $data['heading'] 			= 'Category';
        $data['Sub_heading'] 		= 'Category Manager';
        $data['page_type'] 			= 2	;
        $data['action'] 			= 'category';
        $data['parents']             = Category::where('parent_id',0)->get();

        return view('admin/category/create', $data);
    }

    public function store(Request $request)
    {
        $data = [];
        $data['name'] = $request->name;
        $data['parent_id'] = $request->parent_id;

        Category::create($data);

        return Redirect::to('/category');

    }

    public function edit($id){

        $data['heading'] 			= 'Category Edit';
        $data['Sub_heading'] 		= 'Category Manager';
        $data['page_type'] 			= 2	;
        $data['action'] 			= "category/$id/update";
        $data['categories']         = Category::with('children')->where('parent_id',0)->get();

        return view('admin/category/edit', $data);
    }

    public function update(Request $request, $id)
    {
         $data['name'] = $request->name;
         Category::find($id)->update($data);
        return redirect()->route('category.index');
    }

    public function destroy(Category $category)
    {
        if ($category->children) {
            $category->children()->delete();
        }

        $category->delete();

        return redirect()->route('category.index');
    }
}
