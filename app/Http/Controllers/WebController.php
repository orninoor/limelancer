<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Session;

class WebController  extends Controller
{

	public function __construct() 
	{
		$this->middleware("CheckSession");
	}
	
	public function index()
    {
		$data = array();
		$data['title'] 		= 'Home';
		return view('home',$data);	
    }	
	
	public function gigs()
    {
		$data = array();
		$data['title'] 		= 'GIGS';
		return view('gigs',$data);	
    }	
	
	public function gig_details($id)
    {
		$data = array();
		$data['title'] 		= 'GIGS Details';
		$data['gig_id'] 		= $id;
		return view('gigs_detail',$data);	
    }
}
